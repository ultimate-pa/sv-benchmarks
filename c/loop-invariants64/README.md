<!--
This file is part of the SV-Benchmarks collection of verification tasks:
https://github.com/sosy-lab/sv-benchmarks

SPDX-FileCopyrightText: 2019-2022 Dirk Beyer, Matthias Dangl, Daniel Dietsch, Matthias Heizmann, Thomas Lemberger, and Michael Tautschnig

SPDX-License-Identifier: Apache-2.0
-->

These tasks are adaptations of tasks present in the
[loop-invariants](../loop-invariants/) directory.
These tasks have the difference that they use the `LP64`
data model which is relevant for their verdict.