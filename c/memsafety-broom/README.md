<!--
This file is part of the SV-Benchmarks collection of verification tasks:
https://github.com/sosy-lab/sv-benchmarks

SPDX-FileCopyrightText: 2023 Broom team

SPDX-License-Identifier: GPL-3.0-or-later
-->

This directory contains verification tasks for checking memory safety.
The C programs create, traverse, and destroy different unbounded lists: Single-Linked List, Double-Linked List and Lists from Linux Kernel (`include/linux/list.h`).

Contributed by: [Broom](https://pajda.fit.vutbr.cz/rogalew/broom/) project (Adam Rogalewicz, Veronika Šoková, Tomáš Vojnar, Florian Zuleger, Lukáš Holík)
